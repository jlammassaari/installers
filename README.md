# Building the Docker Images
From the root of dockerinst project.

#### Build install base 
    docker build -t boomi/install:<version> ./src/install
#### Build atom    
    docker build -t boomi/atom:<version> ./src/atom
#### Build molecule    
    docker build -t boomi/molecule:<version> ./src/molecule
#### Build cloud
    docker build -t boomi/cloud:<version> ./src/cloud
    
# Tips and tricks
To see running containers:   
`docker ps`
	
To see all containers:   
`docker ps -a`
	
To see running processes in a container:  
`docker top <container name>`
	
To exec into a running container and obtain a bash session:  
`docker exec -it <container name> /bin/bash`


Example startup:  
`./clouddocker_install64.sh -n dockerCloud -u admin@boomi.com -p boomi -a boomi-internal -c '1ca8f31d-8141-4a2d-a44f-d75f4a0ea428'`    
`./moleculedocker_install64.sh -n dockerMolecule -u admin@boomi.com -p boomi -a boomi-internal`  
`./atomdocker_install64.sh -n dockerAtom -u username@x.com -p xxx -a account1`  